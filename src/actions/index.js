import axios from 'axios';

const API_KEY = ''; //api ne zahtjeva auth

export const GET_CRYPTO_DATA = 'GET_CRYPTO_DATA';
//eventi triggerani od strane usera, ili ajax poziva ili ucitavanja stranice 
//zovu action kreatora
//action kreator je funkcija koja vraca akciju (objekt)
//akcija (objekt) je automatski poslan svim reducerima u aplikaciji
//reduceri mogu odabrat kakvo ce stanje vratiti, ovisno o akciji
//novo vraceno stanje se ubaci u app stanje i app stanje se proslijedi react applikaciji
//koja re-rendera komponentu

//objekt ima type koji opisuje tip akcije koja je triggerana
//i takoder moze imati neke podatke koji opisuju akciju - payload
export function getCryptoData() {
    const url = 'https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,XRP,BCH,EOS,XLM,LTC,ADA,MIOTA,USDT&tsyms=HRK';
    //promise
    const request = axios.get(url);
    return {
        type: GET_CRYPTO_DATA,
        payload: request
    };
}