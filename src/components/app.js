import React, { Component } from 'react';
import CryptoTable from '../containers/crypto_table';

export default class App extends Component {
  render() {
    return (
      <div className="container">
        <CryptoTable />
      </div>
    );
  }
}
