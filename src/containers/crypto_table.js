import React, { Component } from 'react';
//container je komponenta koja ima direktan pristup stanju koje kreira redux
//kako je ovoj komponenti bitno kada se vrijednosti u listi mijenjaju, ona postaje kotenjer (pametna komponenta)
import { connect } from 'react-redux';
//action kreator
import { getCryptoData } from '../actions/index';
//funkcija s kojom se postize da akcija generirana od action kreatora
//dođe do svih reducera
import { bindActionCreators } from 'redux';

class CryptoTable extends Component {
    constructor (props) {
        super(props);
        this.state = { names: [], loading: true }
    }
    
    componentDidMount = () => {
        this.formatCryptoData();
        //
        setInterval(this.formatCryptoData, 5000);
    }

    formatCryptoData = () => {
        this.props.getCryptoData().then(() => {
            //nakon sto se promise resolva
            //takoder ce biti dostupna vrijednost i na cryptodata objektu
            
            //valute
            const names = Object.keys(this.props.cryptodata.data);
            //vrijednosti u HRK
            const cryptoArray = Object.keys(this.props.cryptodata.data).map((i) => {
                return this.props.cryptodata.data[i].HRK;
            });
            this.setState({
                data: cryptoArray,
                names: names,
                loading: false
            });           
        });
    }

    printEachElement = (index) => {
        return this.state.names[index];
    }
    printListItem = () => {
        return this.state.data.map((elem, index) =>
            <li className="list-group-item" key={index}>Valuta: <b>{this.printEachElement(index)}</b> Vrijednost u HRK: <b>{elem}</b> </li>
        );
    }
    render () {
        const { loading } = this.state;
        if (loading) {
            return (
                <p> 
                    Getting data .. 
                </p>
            );
        }
        return ( 
            <div>
                <ul className="list-group">
                    {this.printListItem()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps(state) {
    //svrha ove funkcije je uzeti stanje kao argument
    //i sto se god vrati iz nje, ce biti dostupno na this.props unutar kotenjera

    return {
        cryptodata: state.cryptodata
    }
    //kad se god stanje app promijeni, ovaj kontenjer ce se instantno re-renderat
    //.. i crpytodata objekt ce bit dostupan na this.props
    //sto znaci.. da nije potrebno pisati state gore, automatski ce ga assignat pod this.props
}

// sve vraceno iz ove funkcije ce biti postavljeno kao props na kontenjeru
// dakle, action kreator je isto dostupan pod this.props
function mapDispatchToProps(dispatch) {
    //skraceni zapis prema es6 { getCryptodata }
    //getCryptoData je action kreator
    // kad se god getCryptoData pozove, rezultat treba biti proslijeden
    // svim reducerima
    // dispatch funkcija uzima sve akcije i salje ih svim reducerima
    return bindActionCreators({ getCryptoData: getCryptoData }, dispatch);
}

//      uzmi komponentu i funkciju gore i vrati kontenjer
//      kontenjer je komponenta svjesna stanja koje sadrzi redux
// export default connect(null, mapDispatchToProps)(CryptoTable);

// 
export default connect(mapStateToProps, mapDispatchToProps)(CryptoTable);