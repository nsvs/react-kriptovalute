import { combineReducers } from 'redux';
import CryptoDataReducer from './reducer_cryptodata';
//key je komad stanja, a vrijednost sami reducer
//s ovim se esencijalno kaze reduxu kako kreirati app stanje
//app ima stanje cryptodata a vrijednost sto se god vrati iz cryptodatareducer funkc

//ovaj reducer ce dodat key na globalni app state koje se zove cryptodata
const rootReducer = combineReducers({
  cryptodata: CryptoDataReducer
});

export default rootReducer;
