//reducer je funkcija koja vraća dio stanja aplikacije (vrijednost stanja)

//-- app se moze podijelit na dva dijela, redux (dio koji sadrzi podatke)
// i react, dio koji daje viewove
import { GET_CRYPTO_DATA } from './../actions/index';

//svako stanje u app, ima svoj reducer

//funkcija prima stanje i akciju od action kreatora => (objekt: {type: , payload: })
export default function (state = null, action) {
    //funk provjerava da li treba mijenjati stanje s akcijom
    //ako stanje nije za nju, vrati trenutno stanje (stanje iskljcivo tog reducera)
    console.log('action received ... (reducer)', action);
    switch (action.type) {
        case GET_CRYPTO_DATA: 
            return action.payload
    }
    return state;
}